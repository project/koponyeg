Koponyeg
http://drupal.org/sandbox/k.dani/1462574
====================================

DESCRIPTION
-----------
This simple module will provides weather forecast service from hungarian regions. The displayed data
source come from the hungarian weather forecast portal called koponyeg.hu.
You will have the possibility to choose a region of which you would like to see a weather forecast on your site.

INSTALLATION
-----------
1) Copy koponyeg directory to your modules directory
2) Enable the module at module administration page
4) Configure the Koponyeg weather block and place it in a region on block administer page